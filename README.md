I wrote this REXX for myself, no attempts have been made to "productionize" this.
USE AT YOUR OWN RISK!

This REXX uses SDSF to identify address spaces with *MSTR.
It then parses JESMSGLG to identify:
- Db2 start command
- DSNZPARM member used
- DDF connection information (hostname, DRDA port, DRDA secure port, location)

No attempts have been made to "productionize" this.

How to run it:
It runs like a shell command on z/OS in Unix System Services.
```
chmod +x db2Discover.rexx
./db2Discover.REXX > sampleOutput.json
```


See sampleOutput.json for what the example output looks like.
