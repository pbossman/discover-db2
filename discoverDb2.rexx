/* REXX */
rc=isfcalls('ON')
 /* Set owner to all, show jobs with MSTR suffix */
isfprefix = "*MSTR"
isfowner = "*"
/* Access the DA panel */
Address SDSF "ISFEXEC DA"
if rc<>0 then
 Exit rc
 /* First word is jobname of isfcols special variable */
jobname = word(isfcols,1)
/* Loop through jobs */
ssidCount = 0
lastLine = 'START'
do ix=1 to isfrows
 tempjobname = value(jobname"."ix)
  /* row to list the data sets in the job. */
 Address SDSF "ISFACT DA TOKEN('"TOKEN.ix"') PARM(NP ?)" ,
    "( prefix jds_"
 if rc <> 0 then
   exit rc
 /* Find the JESMSGLG data set and allocate it */
 /* using the SA action character */
 do jx=1 to jds_DDNAME.0
   if jds_DDNAME.jx = "JESMSGLG" then
     do
       Address SDSF "ISFACT DA TOKEN('"jds_TOKEN.jx"')" ,
         "PARM(NP SA)"
       if rc<>0 then
         exit 20
       do kx= 1 to isfddname.0
         Address MVS "EXECIO * DISKR" isfddname.kx "(STEM line. FINIS"
         /* initialize loop variables */
         lx = 1
         quitLoop = 'N'
         /* loop through all JESMSGLG rows, or until we quit */
         do while (lx < line.0 & quitLoop = 'N')
           /* find line that contains DSNZPARM module name */
           if WORD(line.lx,3) = "DSNZ002I" then
             do
               ssidCount = ssidCount + 1
               if ssidCount = 1 then
                 do 
                   Say "["
                 end
               if lastLine = 'DDF' then
                 do
                    Say '    },'
                 end
               Say "    {"
               Say '        "SSID" : "'SUBSTR(tempjobname,1,POS('MSTR',tempjobname)-1)'",'
               Say '        "commandPrefix" : "'WORD(line.lx,4)'",'
               Say '        "DSNZPARM" : "'WORD(line.lx,14)'",'
             end
           if WORD(line.lx,3) = "DSNL004I" then
             do
               contChar = WORD(line.lx,8)
               quitLoop2 = 'N'
               lx = lx + 1
               do while (lx < line.0 & quitLoop2 = 'N')
                 ddfParm = WORD(line.lx,2)
                 /* LOCATION and DOMAIN have quotes around value with a trailing comma  */
                 /* TCPPORT value is integer, so no quotes around value with trailing comma */
                 /* SECPORT value is integer, so no quotes, no trailing comma.  */ 
                 if (ddfParm = 'LOCATION' | ddfParm = 'DOMAIN') then
                   do
                     Say '        "'ddfParm'" : "'WORD(line.lx,3)'",'
                   end
                 if (ddfParm = 'TCPPORT') then                                                                                                   
                   do
                     Say '        "'ddfParm'" : 'WORD(line.lx,3) ','                                                                 
                   end
                 if (ddfParm = 'SECPORT') then
                   do
                     Say '        "'ddfParm'" : 'WORD(line.lx,3)                                                                 
                   end

                 lx = lx + 1
                 if WORD(line.lx,1) /= contChar then
                   do
                     quitLoop2 = 'Y'
                     lastLine = 'DDF'
                   end
               end
             end
           lx = lx + 1
         end
       end
     end
 end
end
if ssidCount > 1 then
  do
    Say "    }"
    Say "]"
  end
rc=isfcalls('OFF') 
EXIT 0
